# Tweetoscope project for the SDI Major 2020-2021
The _tweetoscope_ tool allows to predict the popularity of a tweet in real-time. 

## What is the _tweetoscope_ ?
The _tweetoscope_ receives tweets from a _Generator_ node. These tweets are then processed by a _Collector_ node that groups series of tweets and following retweets in a same _cascade_. The _Estimator_ node then models the _cascade_ using a self-exciting Hawkes process. The estimated parameters of the process are sent to the _Predictor_ which uses it to predict the final total length of a tweet cascade, using a _Random Forest_ regressor provided and trained by the _Learner_ node. Results, including alerts and statistics about predictions are then displayed by a _Dashboard_ and a _Monitor_.
The whole project relies on _Kubernetes_ micro-services interacting though _Kafka_ topics.


## Getting started
### Deploy the tweetoscope on a cluster using Kubernetes
To deploy the _tweetoscope_, you can find the yaml files directly in the YAML_file section of th git repository.
You can also download the different yaml files :
-	yaml file to deploy the middleware (zookeeper and kafka broker): 
```Shell
`wget https://gitlab.com/Mamoun_Alaoui/Tweetoscope/-/jobs/884567490/artifacts/download?file_type=archive`
```
-	yaml file to deploy the tweetoscope services:
```Shell
`wget https://gitlab.com/Mamoun_Alaoui/Tweetoscope/-/jobs/884567491/artifacts/download?file_type=archive`
``` 
-	yaml files to login to pull image from our private repository:
```Shell
`wget https://gitlab.com/Mamoun_Alaoui/Tweetoscope/-/jobs/882107961/artifacts/download?file_type=archive`
```  

A short tutorial containing commands have been provided to be able to pull the containers built from our private container registry on Gitlab. (_YAML_files > pull_image_from_private_git_tutorial.txt_)
All the useful commands required to run the project on a given cluster have been listed on the file _useful_commands.txt_. Please refer to it in case of problem.

### Run docker images
It is also possible to run locally docker images without deployment. To do so, one must:

1. Run zookeeper and kafka broker.
```Shell
- `docker run --rm -it --network host zookeeper`
- `docker run --rm -it --network host --env KAFKA_BROKER_ID=0 --env KAFKA_LISTENERS=PLAINTEXT://:9092 --env KAFKA_ZOOKEEPER_CONNECT=localhost:2181 --env KAFKA_CREATE_TOPICS="tweets:1:1,cascade_series:2:1,cascade_properties:2:1,samples:1:1,alerts:1:1,stats:1:1,logs:1:1,models:1:1" wurstmeister/kafka`
``` 
_NB : We have chosen to set the number of partitions of the topics cascade_properties, cascade_series and tweets to 2 since it is the topics with most traffic. In case one wants to use more observation windows than the ones we use in the demo (600 and 1200), it may be relevant to increase the number of partitions of these topics, in particular for cascade_properties which keys are the observation windows._

2. Run the different images using the command (service being replace by the name of the node needed):
```Shell
docker run --rm -it registry.gitlab.com/mamoun_alaoui/tweetoscope/service
``` 

An environment to manually build the generator and collector executables is also provided. It already contains some important dependances such as the Gaml library and cppkafka. To use it, please type the following command in your command line :
```Shell
docker pull caracal97/env_build
```
For instance, to build the collector executable from scratch, one can use the following command (directly from the collector node path) :
```Shell
make collector
```

## Trigger useful unit tests
A set of unit test have been implemented within the project. The tests are automatically triggered when starting a new CI pipeline on gitlab and a xml report is generated. Two types of tests have been used :
- Python tests using the external module pytest. It concerns the nodes written in python (_estimator, predictor, learner_). These tests assert
 the adequation of the coded structures to an expected behaviour. To do so, some external objects for which the output by the different nodes are known,
 have been provided in the files Test_objects of each python node. Pytest uses these objects to perform the tests.
- Tests for C++ objects (_generator, collector_)


An external person to the project can also perform these unit tests. To run the python-related test, one must first of all install pytest :
```Shell
pip install pytest
``` 
Then, simply run on the root of the project file imported/cloned :
```Shell
pytest
``` 
The report should be automatically displayed at the end.

## Visualize your results using the Dashboard and the Monitor
 
The visualization and monitoring tools: 

The visualization tools is composed of a dashboard and monitor:

**Dashboard:** A tool that dynamically displays the hot-topics posted in the alerts topic, i.e tweets with the highest predicted popularities (alerts topic) using the loki-stack below: 
    - Promtail is the agent, responsible for gathering logs and sending them to Loki 
    - Loki is the main server, responsible for storing logs and processing queries. 
    - Grafana for querying and displaying the logs. 

**Monitor:** Another tool that monitors the performance of your system, i.e the time evolution of prediction errors posted in the stats topic. Using the Prometheus-monitoring stack: 
    - Prometheus: is the agent, responsible for to selecting and aggregating tune series data in real time, select sa  value/message pair to detect alerts (stats topic) 
    - Grafana for querying and displaying the logs. 

## Contributors
- Yasmine Bennani
- Afaf El-Wafi
- Mamoun Alaoui
