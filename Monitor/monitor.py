#Import basic libraries
import numpy as np
import os
import sys

# Import libraries to handle kafka messages
import json                                         # To parse and dump JSON
from kafka import KafkaConsumer                     # Import Kafka consumer
from kafka import KafkaProducer                     # Import Kafka producer
from kafka.admin import KafkaAdminClient, NewTopic  # Create topics

# Import logger
import logger

# Getting the current path of the project
current_path = os.getcwd()

if __name__ == '__main__' :

    # Checking if a parameters file has been passed as an argument in the terminal
    if len(sys.argv) != 2 :
         print("Usage " + sys.argv[0] + " <config-filename>")
         exit()
    else :
        # Getting the parameters from the txt parameters file
        with open(current_path + "/" + str(sys.argv[1])) as f :
            for line in f :
                # Skipping comments, section titles and blank lines
                if line[0] not in ['#','[', ' '] :
                    # The strip method allow to get rid of spaces and quotoation marks (" " and ' ') characters in the begining of the string
                    param = list(map(lambda x: x.strip(' \' \n'), line.split('=')))
                    if param[0] == 'brokers' : brokers = param[1]
                    elif param[0] == 'in_' : in_=param[1]
                    elif param[0] == 'stat_eval_threshold' : threshold=int(param[1])
    
    # Create logger
    logger = logger.get_logger('monitor-node', broker_list=brokers, debug=True) 
    
    # Construction of the Kafka consumer
    consumer = KafkaConsumer(in_,                                          # Topic name
    bootstrap_servers = brokers,                                           # List of brokers
    group_id = 'monitor_nodes'                                         
    )
    # Setting a group_id prevents the monitor from getting the same message twice if the service is parallelized
    # in the cluster.

    old_statistics = {}
    statistics = {}
    counter = {}
    
    for msg in consumer :                                               # Blocking call waiting for a new message                                 
        stat_msg = json.loads((msg.value).decode('utf-8'))              # We deserialize the message
        cid = stat_msg['cid']
        T_obs = stat_msg['T_obs']
        ARE = stat_msg['ARE']

        if T_obs not in statistics.keys() :
            statistics[T_obs] = ARE
            counter[T_obs] = 1

        if T_obs not in old_statistics.keys() :
            old_statistics[T_obs] = np.inf

        elif T_obs in statistics.keys() :
            if counter[T_obs]>=threshold :
                statistics[T_obs] += ARE
                final_ARE = round(statistics[T_obs]/threshold, 2)
                if final_ARE < old_statistics[T_obs] :
                            logger.info("\n ------------------------stats------------------------ \n \
For the time observation {}, the model managed to improve its performances reaching a mean ARE of {} on the {} previous predictions, instead of {} previously.\n \
------------------------------------------------------------- \n".format(T_obs, final_ARE, threshold, old_statistics[T_obs]))
                else :
                            logger.info("\n ------------------------stats------------------------ \n \
Unfortunately, for the time observation {}, on the {} previous predictions, the performances of the model have decreased from a mean ARE of {} to {}.\n \
------------------------------------------------------------- \n".format(T_obs, threshold, old_statistics[T_obs], final_ARE))
                old_statistics[T_obs] = final_ARE
                statistics[T_obs] = 0
                counter[T_obs] = 0
            else :
                counter[T_obs] += 1


