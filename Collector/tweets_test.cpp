/* 

TESTS FOR STRUCTURE tweets
command :  g++ -o tweets_test -O3 -Wall -std=c++17 `pkg-config --libs --cflags gaml cppkafka` -lpthread tweets_test.cpp

*/
#include <gtest/gtest.h>


// Les exceptions standards
// On utilise les classes cascade
#include "tweets.h"

tweetoscope::tweet tweet_ex={"tweet" ,"blah blah ",12,100,1000, 0, "cid=12" };

   TEST(TweetsTest, Msg) {
     ASSERT_EQ(tweet_ex.msg, "blah blah ");
    
}
   TEST(TweetsTest,Info) {
     ASSERT_EQ(tweet_ex.info, "cid=12");
    
}
   TEST(TweetsTest, Cascade) {
     ASSERT_EQ(tweet_ex.cascade, 12);
    
}
   TEST(TweetsTest, Source) {
     ASSERT_EQ(tweet_ex.source, 0);
    
}
   TEST(TweetsTest, Magnitude) {
     ASSERT_EQ(tweet_ex.magnitude, 1000);
    
}
   TEST(TweetsTest, Time) {
     ASSERT_EQ(tweet_ex.time, 100);
    
}
  TEST(TweetsTest, Type) {
     ASSERT_EQ(tweet_ex.type, "tweet");
    
}


/************* Test Runner Code goes here **************/
int main(int argc, char **argv) {

        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
