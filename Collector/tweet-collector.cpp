#include <iostream>
#include <ostream>
#include <sstream>
#include <map>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <string>
#include <memory>
#include "Processor.h"


int main(int argc, char* argv[]) {
   Program program;

	// Read messages
  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " params.config" << std::endl;
    return 0;
  }
  tweetoscope::params::collector params(argv[1]);
  // Consumers config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", params.kafka.brokers},
		{ "auto.offset.reset", "earliest" },
		{ "group.id", "myOwnPrivateGroup" }
	};

	// Producers config
	cppkafka::Configuration configp = {
		{ "bootstrap.servers", params.kafka.brokers},
        
	};


	// Create the consumerand the producers
	cppkafka::Consumer consumer(config);
	cppkafka::Producer producer(configp);
	consumer.subscribe({params.topic.in});
	cppkafka::MessageBuilder builder(params.topic.out_series);




  	// Read messages
  	std::cout<< "Configuration is done" <<std::endl;
	while(true) {
	  auto msg = consumer.poll();
	  if( msg && ! msg.get_error() ) {
	    tweetoscope::tweet twt;

	    auto key = tweetoscope::cascade::idf(std::stoi(msg.get_key()));
	    auto istr = std::istringstream(std::string(msg.get_payload()));
	    istr >> twt;
	    // We read msg it's cool let's decide what to do with
	    auto t_source= twt.source; // We have the source here
	    auto t_cascade=twt.cascade;
	    auto t_time=twt.time;
	    auto t_msg=twt.msg;

        program+={t_source,params.times.observation,params.times.terminated,params.cascade.min_cascade_size};

     	// Add a processor with the corrensponding source
        program+={t_source,t_time,t_cascade,twt,params.topic.out_properties, params.topic.out_series,producer};
	  }

	  }
	  return 0;

}

