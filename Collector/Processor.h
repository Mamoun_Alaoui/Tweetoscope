#include "cascade.h"
#include "tweetoscopeCollectorParams.h"
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <tuple>
#include <string>
#include <stdexcept>
#include <string>
#include <sstream>
#include <cppkafka/cppkafka.h>

///////////////
//           //
// Processor //
//           //
///////////////

struct Processor {

  using source=tweetoscope::source::idf;
  using timestamp=tweetoscope::timestamp;
  using idf=tweetoscope::cascade::idf;
  using ref = std::shared_ptr<Cascade>;

  source  src;                                        // Source of a processor
  priority_queue cas_queue;                           // Cascades queue
  std::string name="Processor_";
  timestamp time;                                     // Last modification time
  std::map<timestamp, std::queue<std::weak_ptr<Cascade>>> fifo;
  std::map<idf,std::weak_ptr<Cascade>>cascades;
  std::vector<std::size_t> observations;
  std::size_t  terminated;
  std::size_t min_cascade_size;

  // Create a processor from a source
  Processor(const source& sc,const timestamp& tm) : src(sc),time(tm){
    // Add the weak pointer in FIFOS
    terminated=0;
    min_cascade_size=0;
    name+=std::to_string(sc);
    }
};



// Define print operator for processor
std::ostream& operator<<(std::ostream& os, const Processor& m) {
  os << "{\"source\": \"" << m.src << "\", \"name\": " << m.name << "\", \"time\": " << m.time;
  if(!m.cascades.empty()) {                            // In the cascades
  os << ", \"cascades\": [";
    for (auto val= m.cascades.begin();val!=m.cascades.end();++val){
        auto cascade=val->second;
  }
  os << "]}"<<std::endl;
  }
  else{
    os << "\", \"cascades\": []}"<<std::endl;

  }
    return os;
}

// We define a profram sructure including processors 
struct Program {
  using idf=tweetoscope::cascade::idf;
  using timestamp=tweetoscope::timestamp;
  using source=tweetoscope::source::idf;
  using ref  = std::shared_ptr<Cascade>;

  // Allocate a source to each processor
  std::map<source, Processor> program;

  // Add a processor if it does not exist according to a source and add times
  void operator+=(const std::tuple<source,std::vector<std::size_t>,std::size_t,std::size_t>& data) { // Arg = {module_name, teacher, coefficient}
     
      auto [it, is_newly_created] = program.try_emplace(std::get<0>(data),std::get<0>(data),0);

    // Just created so let's initialize its values
    if(is_newly_created){ 
        auto proces=it->second;
        proces.observations=std::get<1>(data);
        proces.terminated=std::get<2>(data);
        proces.min_cascade_size=std::get<3>(data);
        std::queue<std::weak_ptr<Cascade>> fif;
        for(auto time: proces.observations ){
            auto it=proces.fifo.try_emplace(time,fif);
        }
        program.at(std::get<0>(data))=proces;
    }

    // Already exists 
    else{
        auto proces=it->second;
        proces.observations=std::get<1>(data);
        proces.terminated=std::get<2>(data);
        proces.min_cascade_size=std::get<3>(data);
        proces.observations=std::get<1>(data);
        program.at(std::get<0>(data))=proces;
        }
    }




  // Add a processor if it doesn't exist according to a source and a cascade shared pointer config  
  void operator+=(const std::tuple<source,timestamp,idf,tweetoscope::tweet,std::string,std::string,cppkafka::Producer&>& data) {
    
    // Set up kafka brokers 
    cppkafka::MessageBuilder builder_p(std::get<4>(data));
    cppkafka::MessageBuilder builder_c(std::get<5>(data));
   
   
    // Get the process working on the current source
    auto process=program.at(std::get<0>(data));
    // Let's check if the process has already a cascade with idf
    auto it =process.cascades.find(std::get<2>(data));
    
    
    // No cascade with this idf two possibilities either deleted either never created so let's create one if we have 
    // a new tweet
    if (it==process.cascades.end()){ 
         if (std::get<3>(data).type=="tweet"){

           // Create a cascade
            Cascade new_cascade(std::get<2>(data));

            // We will add a tweet to cascade
            new_cascade=std::get<3>(data);

            // Shared pointer of cascades
            auto ref_casca = std::make_shared<Cascade>(new_cascade);

            // We will emplace the ponter in cascades
            process.cascades[std::get<2>(data)]=ref_casca; 

            // Add a shared pointer in the queue
            ref_casca->location = process.cas_queue.push(ref_casca);

            // Add the weak ptr in fifos
            for(auto time: process.observations ){
              process.fifo.at(time).push(ref_casca);
            }
           program.at(std::get<0>(data))=process;
      }
       program.at(std::get<0>(data))=process;
    }


    // Cascade exists so it is already in the queue
    else{

      // Shared pointer of the cascade
      auto ref_cascade=it->second.lock();

      // Previous time of the cascade
      auto prev_time=ref_cascade->getime();

      auto prev_time_init=ref_cascade->getcascade().front().first;

      // Next time if we change it
      auto next_time=std::get<3>(data).time;

      // Add the weak ptr in FIFOS
      // For each FIFO
      for(auto time: process.observations ){

          // We get the cascade FIFO at time of observation t
          auto current_queue=process.fifo.at(time);

          // While the current queue is not empty
          while (!current_queue.empty()){

              // For each partial cascade we check the first cascade
              auto current_cas=current_queue.front();
              auto ob =current_cas.lock();

              // We chech time constraint
              if (next_time-prev_time_init>=time){

                   // We delete it from FIFO
                  process.fifo.at(time).pop();

                  // Check size constraint
                  if ((current_cas.lock())->getsize()>process.min_cascade_size){
                     std::ostringstream ostr;
                     sendSerie(ostr,*(current_cas.lock()),time);
                     std::string message {ostr.str()};
                     builder_c.payload(message);
				            (std::get<6>(data)).produce(builder_c);
                   }
               }
                current_queue.pop();
           }
       }

      *ref_cascade=std::get<3>(data);

      program.at(std::get<0>(data))=process;
      if(!process.cas_queue.empty()){
         if(next_time-prev_time>process.terminated){
            auto to_send=process.cas_queue.top();
            process.cascades.erase(std::get<2>(data));
            program.at(std::get<0>(data))=process;
            // Send it
            if (to_send->getsize()>process.min_cascade_size){
                std::ostringstream ostr;
                sendPropertie(ostr,*(ref_cascade));
                std::string message {ostr.str()};
                for(auto time: process.observations ){
                   std::string str_time=std::to_string(time);
                   builder_p.key(str_time).payload(message);
                    (std::get<6>(data)).produce(builder_p);}
              }
        }


       }
      // Set up program
       program.at(std::get<0>(data))=process;

    }
    
    program.at(std::get<0>(data))=process;


}


// This removes a process
void operator-=(const source& src) {
    if(auto it = program.find(src); it != program.end())
      program.erase(it);
  }

};

std::ostream& operator<<(std::ostream& os, const Program& prc) {
  os << "Program : " << std::endl;
  // name_processor : pair(source,processor)
  for(auto& name_processor: prc.program)
    os << "  " << name_processor.first << " : " <<name_processor.second << std::endl;
  return os;
}


