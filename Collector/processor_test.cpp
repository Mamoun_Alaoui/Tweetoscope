/* 

TESTS FOR CLASS  Processor
command :  g++ -o processor_test -O3 -Wall -std=c++17 `pkg-config --libs --cflags gaml cppkafka` -lpthread processor_test.cpp


*/

// Les exceptions standards
// On utilise les classes cascade
#include "Processor.h"
#include <gtest/gtest.h>

 
 TEST(ProcessTest, ADD) {
    Program program;
    std::vector<size_t>obs({123,45});
    program+={0,obs,2000,10};
    ASSERT_TRUE(true);
    
}

 TEST(ProcessTest, Remove) {
    Program program;
    program-={0};
    program-={0};
    ASSERT_TRUE(true);
 }



/************* Test Runner Code goes here **************/
int main(int argc, char **argv) {

        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
