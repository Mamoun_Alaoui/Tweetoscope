/////////////
//         //
// Cascade //
//         //
/////////////

/*
Cascade is the class where we strore tweeets with the same cascade id 
*/


#include "tweets.h"
#include <memory>
#include <boost/heap/binomial_heap.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <sstream>


class Cascade; 

// Namespace to use 
using cascade_ref  = std::shared_ptr<Cascade>;
using idf=tweetoscope::cascade::idf;
using timestamp=unsigned int;


struct cascade_ref_comparator {
  bool operator()(cascade_ref op1, cascade_ref op2) const; // Defined later.
};

// Defining the priority queue 
using priority_queue = boost::heap::binomial_heap<cascade_ref,
                          boost::heap::compare<cascade_ref_comparator>>;


// This is the actual class definition.
class Cascade {
     using timestamp=unsigned int;
 
 
 
 private:
  int cas_size;
  timestamp  time;// Timestamp of cascade last time
  timestamp Tobs;
  idf value;///identifier of cascade
  std::vector<std::pair<timestamp, double>> tweets;///retweeets

  public:
  priority_queue::handle_type location; // This is "where" the element
                    // is in the queue. This is
                    // needed when we change the
                    // priority.

  Cascade(idf valu): value(valu), cas_size(0) ,Tobs(0){} // Constructor

  ////Getters 
  timestamp getime(){return time;}
  void setTobs(timestamp t){Tobs=t;}
  int getsize(){return cas_size;}
  idf getvalue(){return value;}
  std::vector<std::pair<timestamp, double>> getcascade(){return tweets;}


  // Operators 
  bool operator<(const Cascade& other) const {return time < other.time;}

  void operator=(tweetoscope::tweet retweet)
  {time = retweet.time;
  tweets.emplace_back(retweet.time,retweet.magnitude);
  cas_size=cas_size+1;
  }

  /// Friends operators
  friend  std::ostream& sendSerie(std::ostream& os,const Cascade& e,timestamp tobs);
  friend  std::ostream&  sendPropertie(std::ostream& os, const Cascade& e);
  friend std::ostream& operator<<(std::ostream& os, const Cascade& e);
};





// Send cascade informations to osstream 
std::ostream& sendSerie(std::ostream& os,const Cascade& e,timestamp tobs){
    os<<"{ \"type\": \'serie\', \"cid\": "<<std::to_string(e.value)<<", \"msg\": \'None\', \"T_obs\": "<<std::to_string(tobs)<<", \"tweets\": "<<"[";
    int i=0;
    for(auto twt:e.tweets){
        if (i<e.tweets.size()-1){ os<<"("<<std::to_string(twt.first)<<","<<std::to_string(twt.second)<<"),";}
        else {os<<"("<<std::to_string(twt.first)<<","<<std::to_string(twt.second)<<")]";}
        i++;
      }
    os<<"}";
    return os;
}


// Send properties od the cascade to ostream
std::ostream&  sendPropertie(std::ostream& os, const Cascade& e){
    os<<"{ \"type\": \"size\", \"cid\": "<<e.value<<", \"n_tot\": "<<e.cas_size<<", \"t_end\": "<<e.time<<" }";
    return os ;

}



// Send cascade to ostream with operator <<
std::ostream& operator<<(std::ostream& os, const Cascade& e) {
  os <<"{\"cascade_id\":"<< e.value<<","<<"\"tweets\":"<<"{";
  int i=0;
  for(auto twt:e.tweets){
    if (i<e.tweets.size()-1){ os<<"{\"time\":" << std::to_string(twt.first) <<",\"magnitude\":" << std::to_string(twt.second) << "},";}
    else {os<<"{\"time\":" << twt.first <<",\"magnitude\":" << twt.second << "}}";}
    i++;
  }

  return os;
}


// Now, we know how Cascade is made, we can write the comparison
// functor from < implemented in Cascade
bool cascade_ref_comparator::operator()(cascade_ref op1, cascade_ref op2) const {
  return *op1 < *op2;
}

// This is a convenient function for pointer allocation.
cascade_ref cascade(int value, const std::string msg,int tm) {return std::make_shared<Cascade>(value);}



///Display priority queue
void showpq(priority_queue  gq)
{
    priority_queue  g = gq;
    while(!g.empty()) {
    auto ref = g.top();
    g.pop();
  }
};


