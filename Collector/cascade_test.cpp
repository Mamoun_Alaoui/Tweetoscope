/* 

TESTS FOR CLASS CASCADE
command :  g++ -o cascade_test -O3 -Wall -std=c++17 `pkg-config --libs --cflags gaml cppkafka` -lpthread cascade_test.cpp


*/

// Les exceptions standards
// On utilise les classes cascade
#include "cascade.h"
#include <gtest/gtest.h>


 TEST(CascadeTest, GetSeize) {
    Cascade cas(0);
    ASSERT_EQ(cas.getsize(), 0);
    
}
 TEST(CascadeTest, GetValue) {
    Cascade cas(0);
    ASSERT_EQ(cas.getvalue(), 0);
    
}

 TEST(CascadeTest, AddTweet) {
	tweetoscope::tweet stweet={"tweet" ,"blah blah ",3,5,1000, 0, "cid=12" };
    Cascade cas(12);
	cas= stweet;
    ASSERT_EQ(cas.getsize(), 1);
	ASSERT_EQ(cas.getime(), stweet.time);
    
}

TEST(CascadeTest, Camparaison) {
    Cascade cas(0);
	Cascade cas_bis(0);
	tweetoscope::tweet tweet1={"tweet" ,"blah blah ",12,100,1000, 0, "cid=12" };
	tweetoscope::tweet tweet2={"tweet" ,"blah blah ",12,1000,1000, 0, "cid=12" };
    cas=tweet1;
	cas=tweet2;
	cas_bis=tweet1;
	ASSERT_TRUE(cas_bis<cas);
    
}


TEST(CascadeTest, PriorityQueue) {
	std::vector<std::string> names = {std::string("zero"), "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
	tweetoscope::tweet retweet={"tweet" ,"blah blah ",3,5,1000, 0, "cid=12" };
    priority_queue queue;
	priority_queue::handle_type location; 
	int v = 0;
	cascade_ref two;
	for(const auto& name : names) {
		auto ref = cascade(v, name,v);
		if(v == 3) two = ref; 
		ref->location = queue.push(ref);
		v+=1;
	}

	*two = retweet;
	queue.increase(two->location, two);
	while(!queue.empty()) {
		auto ref = queue.top();
		queue.pop();
		std::cout << *ref << std::endl;
	}
	ASSERT_TRUE(true);
    
}




/************* Test Runner Code goes here **************/
int main(int argc, char **argv) {

        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
