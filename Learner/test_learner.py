# Import testing package
import pytest

# Import basic libraries
import pandas as pd
import numpy as np
import sklearn

# Import learner
from learner import Learner

# Initialize params used for the tests
samples = pd.read_csv('./Predictor/Test_objects/samples_test.csv', index_col=0)

# Class to perform unit tests on the learner methods
class TestPLearner:

    @pytest.fixture(name="learner_inst")
    def learner_fixture(self):
        return Learner(600, samples, 'RandomForest')

    # Test learner constructor
    def test_constructor(self, learner_inst):
        assert isinstance(learner_inst, Learner)
        assert isinstance(learner_inst.model, sklearn.ensemble._forest.RandomForestRegressor)

    # Test learner fit method
    def test_fit(self, learner_inst):

        def is_fitted(model):
            '''
            Checks if a scikit-learn estimator/transformer has already been fit.
            Parameters
            ----------
            model: scikit-learn estimator (e.g. RandomForestClassifier) 
                or transformer (e.g. MinMaxScaler) object
            Returns
            -------
            Boolean that indicates if ``model`` has already been fit (True) or not (False).
            '''
            attrs = [v for v in vars(model)
                    if v.endswith("_") and not v.startswith("__")]
            
            return len(attrs) != 0
        learner_inst.fit()
        assert isinstance(learner_inst.model, sklearn.ensemble._forest.RandomForestRegressor)
        assert is_fitted(learner_inst.model)