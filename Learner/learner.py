# Import basic libraries
import numpy as np
import sys
import os
import pandas as pd  

# Import libraries to handle kafka messages
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producer
import pickle                     # Library to save and load ML regressors using pickle
from joblib import dump, load

# Import scikit learn for machine learning model
import sklearn
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV

# Import logger
import logger

# Getting the current path of the project
current_path = os.getcwd()

class Learner :
    """
    Constructs a learner object, caracterized by the observation windows T_obs, the samples dataset attached to it,
    and the type of model to be used to fit the data

    Args:
        self : the learner object
        T_obs : int
            the observation window (ex: 600, 1200...)
        samples: pandas DataFrame
            the samples used to train the model
        model_type : str
            the type of model used to fit the data ('RandomForest' or 'GradientBoosting" are implemented so far)
        counter : int
            counter to check it the threshold to learn has been reached
        

    Return:
        The learner object constructed
    """
    def __init__(self, T_obs, samples, model_type, counter=1):
        self.T_obs = T_obs
        self.samples = samples
        if model_type == 'RandomForest' :
            self.model = RandomForestRegressor()
        elif model_type == 'GradientBoosting' :
            self.model = GradientBoostingRegressor()
        self.counter = counter

    # Function to reset the model once it has been sent to the predictor
    def _reset_model(self) :
        if isinstance(self.model, sklearn.ensemble._forest.RandomForestRegressor) :
            self.model = RandomForestRegressor()
        elif isinstance(self.model, sklearn.ensemble._gb.GradientBoostingRegressor) :
            self.model = GradientBoostingRegressor()
    
    # Function used to fit the model used a grid-search cross-validation
    def fit(self):
        grid_parameters = {'max_depth': [10, 25, 50, 100],
                            'min_samples_leaf': [1, 2, 4],
                            'min_samples_split': [2, 5, 10]
                            }
        # We have to reset the previously trained model in order to fit it again
        self._reset_model()
        reg = GridSearchCV(self.model, grid_parameters, cv=5)
        X = self.samples.iloc[:, :-1].values
        y = self.samples.iloc[:, -1].values
        reg.fit(X, y)
        self.model = reg.best_estimator_


if __name__ == '__main__' :
    # We get the parameters config using the config txt file
    if len(sys.argv) != 2 :
         print("Usage" + sys.argv[0] + " <config-filename>")
         exit()
    else :
        with open(current_path + "/" + str(sys.argv[1])) as f :
            for line in f :
                if line[0] not in ['#','[', ' '] :
                    param = list(map(lambda x: x.strip(' \' \n'), line.split('=')))
                    if param[0] == 'brokers' : brokers = param[1]
                    elif param[0] == 'in_' : in_ = param[1]
                    elif param[0] == 'out_' : out_ = param[1]
                    elif param[0] == 'treshold_to_learn' : treshold_to_learn = float(param[1])
                    elif param[0] == 'model_type' : model_type = param[1]


    # Create logger
    logger = logger.get_logger('learner-node', broker_list=brokers, debug=True) 
    
    consumer = KafkaConsumer(in_,                                # Topic name
    bootstrap_servers = brokers,                                 # List of brokers
    key_deserializer= lambda v: int(v.decode()),                 # How to deserialize a key (if any)
    value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize sample messages
    group_id = 'learner_nodes'
    )
    # Setting a group_id prevent the learners from getting the same message twice if the service is parallelized
    # in the cluster.

    producer = KafkaProducer(
    bootstrap_servers = brokers,                                # List of brokers passed from the command line
    key_serializer=str.encode,                                  # How to serialize the key
    value_serializer=lambda v: pickle.dumps(v)                  # How to serialize a model
    )

    learners={}                                                 # For each key a learner

    for msg in consumer :
        samples_msg = msg.value                                   
        key = str(msg.key)
        X = samples_msg['X']
        y = samples_msg['W']
        sample = pd.DataFrame(np.array(X + [y]).reshape(1, -1), columns=['beta','n_star','G1', 'W'])

         # If the key (T_obs) has never been observed before, it is created in the learner dict
        if key not in learners.keys() :
            learner = Learner(key, sample, model_type)
            learners[key] = learner

        else :
            # Appending the sample received in the samples dataset
            learners[key].samples = learners[key].samples.append(sample, ignore_index=True)
            # Incrementing the counter
            learners[key].counter += 1
            # If we reach the threshold : time to learn
            if learners[key].counter >= treshold_to_learn :
                # Time to learn
                learners[key].fit()
                # We reset the counter after learning
                learners[key].counter = 0
                producer.send(out_, key=key, value=learners[key].model)
                logger.info("\n ------------------------model message------------------------ \n \
Model for observation window {} succesfully sent to the predictor \n \
------------------------------------------------------------- \n".format(key))