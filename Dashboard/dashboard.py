#Import basic libraries
import numpy as np
import os
import sys

# Import libraries to handle kafka messages
import json                                         # To parse and dump JSON
from kafka import KafkaConsumer                     # Import Kafka consumer
from kafka import KafkaProducer                     # Import Kafka producer
from kafka.admin import KafkaAdminClient, NewTopic  # Create topics

# Import logger
import logger

# Getting the current path of the project
current_path = os.getcwd()

if __name__ == '__main__' :

    # Checking if a parameters file has been passed as an argument in the terminal
    if len(sys.argv) != 2 :
         print("Usage " + sys.argv[0] + " <config-filename>")
         exit()
    else :
        # Getting the parameters from the txt parameters file
        with open(current_path + "/" + str(sys.argv[1])) as f :
            for line in f :
                # Skipping comments, section titles and blank lines
                if line[0] not in ['#','[', ' '] :
                    # The strip method allow to get rid of spaces and quotoation marks (" " and ' ') characters in the begining of the string
                    param = list(map(lambda x: x.strip(' \' \n'), line.split('=')))
                    if param[0] == 'brokers' : brokers = param[1]
                    elif param[0] == 'in_' : in_=param[1]
    
    # Create logger
    logger = logger.get_logger('dashboard-node', broker_list=brokers, debug=True) 
    
    # Construction of the Kafka consumer
    consumer = KafkaConsumer(in_,                                          # Topic name
    bootstrap_servers = brokers,                                           # List of brokers
    group_id = 'dashboard_nodes'                                         
    )
    # Setting a group_id prevent the dashboard from getting the same message twice if the service is parallelized
    # in the cluster.

    for msg in consumer :                                               # Blocking call waiting for a new message                                 
        alert_msg = json.loads((msg.value).decode('utf-8'))             # We deserialize the message
        cid = alert_msg['cid']
        T_obs = alert_msg['T_obs']
        n_tot = alert_msg['n_tot']

        logger.info("\n ------------------------alert------------------------ \n \
After {} minutes looking at the tweet {}, it is expected to be viral, reaching {} retweets\n \
------------------------------------------------------------- \n".format(round(T_obs//60), cid, n_tot))


