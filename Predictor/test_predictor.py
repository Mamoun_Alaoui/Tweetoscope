# Import testing package
import pytest

# Import basic libraries
import pandas as pd
import numpy as np
import joblib

# Import predictor
from predictor import Predictor

# Initialize params used for the tests
parameters_msg = {'type': 'parameters', 'cid': 'tw23981', 'msg': 'blah blah', 'n_obs': 8, 'n_supp': 0, 
                'params': [0.004253895495732159, 0.006503700725661497, 0.1488863423506256, 0.5017679590126403]}
size_msg = { 'type' : 'size', 'cid': 'tw23981', 'n_tot': 127, 't_end': 4329 }
samples = pd.read_csv('./Predictor/Test_objects/samples_test.csv', index_col=0)
model = joblib.load('./Predictor/Test_objects/model_test.joblib')

# Class to perform unit tests on the predictor methods
class TestPredictor:

    @pytest.fixture(name="predictor_inst")
    def predictor_fixture(self):
        return Predictor(600, model, 1000)

    # Test predictor constructor
    def test_constructor(self, predictor_inst):
        assert isinstance(predictor_inst, Predictor)

    # Test predictor create_sample method
    def test_create_sample(self, predictor_inst):
        X, W_true = predictor_inst.create_sample(parameters_msg, size_msg)
        assert type(X) == list and type(W_true) == float
        assert (list(map(lambda v:round(v, 4), X)), round(W_true, 4)) == ([0.0065, 0.1489, 0.5018], 201.8513)

    # Test predictor predict method
    def test_predict(self, predictor_inst):
        n_tot_predicted = predictor_inst.predict(parameters_msg)
        assert type(n_tot_predicted) == int
        assert n_tot_predicted == 12

    # Test predictor compute_stat method
    def test_compute_stat(self, predictor_inst):
        ARE = predictor_inst.compute_stat(parameters_msg, size_msg)
        assert type(ARE) == float
        assert round(ARE, 5) == 0.90551