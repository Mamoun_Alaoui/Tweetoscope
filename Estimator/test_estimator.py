# Import testing package
import pytest

# Import basic libraries
import pandas as pd
import numpy as np

# Import estimator and Hawkes process
from estimator import Estimator
from hawkes_process import loglikelihood, compute_MAP, compute_MLE, prediction

# Initialize params used for the tests
p, beta = 0.02, 1/3600.
alpha, mu = 2.4, 10
m0 = 1000
prior_params = [ 0.02, 0.0002, 0.01, 0.001, -0.1]
t=60000
tmin = 50
cascade = pd.read_csv('./Estimator/Test_objects/cascade_test.csv', index_col=0).values.reshape(-1, 2)
cascade_msg = {'type' : 'serie', 'cid': 'tw23981', 'msg' : 'blah blah', 'T_obs': 600, 'tweets': [ (102, 1000), (150,127), (150,12), (150,12),\
    (175,13), (224,1665), (312,8), (443,98)] }

# Class to perform unit tests on the functions related to the hawkes process
class TestHawkes:

    # Test loglikelihood function
    def test_loglikelihood(self):
        t_ = cascade[-1,0]
        print(t_)
        print((p,beta))
        LL2 = loglikelihood((p,beta), cascade, t_)
        assert round(LL2) == -20098


    # Test compute_MLE function
    def test_compute_MLE(self) :
        n_star = p * mu * (alpha - 1) / (alpha - 2)
        if n_star < 1:
            n = len(cascade)
            t = cascade[-1,0]

            LL = loglikelihood((p, beta), cascade, t)
            LL_MLE,MLE = compute_MLE(cascade, t, alpha, mu)
            p_MLE, beta_MLE = MLE
            n_star_MLE = p_MLE * mu * (alpha - 1) / (alpha - 2)  
            rel_error = (np.abs(p_MLE - p) + np.abs(beta_MLE - beta) / beta)/2.

        assert (round(LL/n, 4), round(LL_MLE/n, 4), round(p, 5), round(p_MLE, 5), round(beta, 5), round(beta_MLE, 5), \
                        round(n_star, 5), round(n_star_MLE, 5), round(rel_error, 2)) == (-2.0098, -1.8344, 0.02, 0.02857, 0.00028, 0.00082, 0.7, 1.0, 0.98)


    # Test compute_MAP function
    def test_compute_MAP(self) :
        MAP = compute_MAP(cascade, t, alpha, mu, prior_params)[0]
        assert round(MAP, 3) == -19008.938

    # Test prediction function
    def test_prediction(self) :
        pred = prediction((p,beta), cascade, alpha, mu,tmin * 60)[0]
        assert int(pred) == 10063


# Class to perform unit tests on the estimator object methods
class TestEstimator :

    @pytest.fixture(name="estimator_inst")
    def estimator_fixture(self):
        return Estimator('kafka-service:9092', 'cascade_series', 'cascade_properties', prior_params, 1, alpha, mu, 'MAP')


    # Test estimator constructor
    def test_constructor(self, estimator_inst):
        assert isinstance(estimator_inst, Estimator)

    # Test estimator process_cascade method
    def test_process_cascade(self, estimator_inst):
        T_obs, parameters_msg = estimator_inst.process_cascade(cascade_msg)
        assert type(T_obs) == int
        assert list(parameters_msg.keys()) == ['type', 'cid', 'msg', 'n_obs', 'n_supp', 'params']
        assert (type(parameters_msg['type']), type(parameters_msg['cid']), 
                type(parameters_msg['msg']), type(parameters_msg['n_obs']), 
                type(parameters_msg['n_supp']), type(parameters_msg['params'])) == (str, str, str, int, int, list)\
                    and len(parameters_msg['params']) == 4
        assert parameters_msg['type'] == 'parameters'